package avenuecodetest;

import java.util.ArrayList;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class MyTasksScreen {
	
	static WebDriver driver;
	static 
	{
		System.setProperty("webdriver.gecko.driver", "c:\\geckodriver\\geckodriver.exe");
		driver = new FirefoxDriver();

	}
	public MyTasksScreen()
	{			}
	
	@Given("I am logged in ToDo App")
	public void I_am_logged_in_ToDo_App(){
		I_am_on_My_Tasks_screen();
		if(driver.getCurrentUrl().equals("http://qa-test.avenuecode.com/users/sign_in"))
		{	
			WebElement userEmail = driver.findElement(By.id("user_email"));
	        userEmail.sendKeys("mauricio.imamura@gmail.com");
	        WebElement userPassword = driver.findElement(By.id("user_password"));
	        userPassword.sendKeys("sf2sag@123");
	        WebElement signInButton = driver.findElement(By.xpath("//input[@value='Sign in']"));
	        signInButton.click();
		}
	}
    
	@Given("I am on 'My Tasks' screen")
	public void I_am_on_My_Tasks_screen() {
        driver.get("http://qa-test.avenuecode.com/tasks");        
    }
	@Given("'My Tasks' screen doesn't have any task")
	public void My_Tasks_screen_doesnt_have_any_task() {
		ArrayList<WebElement> elements;
		
		elements = (ArrayList<WebElement>)driver.findElements(By.xpath("//button[@ng-click='removeTask(task)']"));
		long begin = System.currentTimeMillis();
		long timeout = 2000;
		while(elements.isEmpty() &&  (System.currentTimeMillis()-begin)<timeout)
		{
			elements = (ArrayList<WebElement>)driver.findElements(By.xpath("//button[@ng-click='removeTask(task)']"));
		}
		if(elements.isEmpty()==false)
			for(WebElement element : elements)
				element.click();
	
    }
	
	@When("I click the add task button")
	public void I_click_the_add_task_button()
	{
        WebElement AddTaskButton = driver.findElement(By.xpath("//span[@ng-click='addTask()']"));
        AddTaskButton.click();	
	}
	@When("I fill task text box with '(.*)'")
	public void I_fill_task_text_box_with(String task)
	{
		WebElement newTask = driver.findElement(By.id("new_task"));
        newTask.sendKeys(task);
	}
	
	@When("I press enter when task text box has the cursor")
	public void I_press_enter_when_task_text_box_has_the_cursor()
	{
		WebElement newTask = driver.findElement(By.id("new_task"));
        newTask.sendKeys(Keys.ENTER);
	}
	
	@Then("'My Tasks' screen continues with no task")
	public void My_Tasks_screen_continues_with_no_task()
	{
		ArrayList<WebElement> elements;
		elements = (ArrayList<WebElement>)driver.findElements(By.xpath("//button[@ng-click='removeTask(task)']"));
		long begin = System.currentTimeMillis();
		long timeout = 2000;
		while(elements.isEmpty() &&  (System.currentTimeMillis()-begin)<timeout)
		{
			elements = (ArrayList<WebElement>)driver.findElements(By.xpath("//button[@ng-click='removeTask(task)']"));
		}
			
		elements = (ArrayList<WebElement>)driver.findElements(By.xpath("//tr[@ng-repeat='task in tasks']"));
        Assert.assertTrue(elements.isEmpty());
	}
	
	@Then("there is one task whose description is '(.*)'")
	public void there_is_one_task_whose_description_is(String description)
	{
		ArrayList<WebElement> elements = (ArrayList<WebElement>)driver.findElements(By.xpath("//tr[@ng-repeat='task in tasks']"));
        Assert.assertTrue(elements.isEmpty()==false && elements.size()==1);
        Assert.assertTrue(elements.get(0).findElement(By.xpath("//td/a[@onaftersave='saveTask(task)']")).getText().equals(description));
	}
	/*@After
	public void closeBrowser()
	{
		driver.quit();
	}*/
}
